import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { books } from '../books';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {
book;

addToCart(product) {
  window.alert('Your product has been added to the cart');
  this.cartService.addToCart(product);
}
  constructor(
    private route: ActivatedRoute,
    private cartService: CartService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.book = books[+params.get('bookId')];
    });
  }

}
