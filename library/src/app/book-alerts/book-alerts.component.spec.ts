import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookAlertsComponent } from './book-alerts.component';

describe('BookAlertsComponent', () => {
  let component: BookAlertsComponent;
  let fixture: ComponentFixture<BookAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
