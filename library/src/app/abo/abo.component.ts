import { Component, OnInit } from '@angular/core';
import { CartService} from '../cart.service';

@Component({
  selector: 'app-abo',
  templateUrl: './abo.component.html',
  styleUrls: ['./abo.component.css']
})
export class AboComponent implements OnInit {
aboCosts;
  constructor(
    private  cartService: CartService
  ) { }

  ngOnInit() {
    this.aboCosts = this.cartService.getAbo();
  }

}
