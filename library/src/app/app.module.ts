import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { BookListComponent } from './book-list/book-list.component';
import { ReactiveFormsModule} from '@angular/forms';
import { BookAlertsComponent } from './book-alerts/book-alerts.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { CartComponent } from './cart/cart.component';
import { AboComponent } from './abo/abo.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: BookListComponent },
      { path: 'books/:bookId', component: BookDetailsComponent},
      { path: 'cart', component: CartComponent},
      { path: 'abo', component: AboComponent },
    ])
  ],
  declarations: [
    AppComponent,
    TopBarComponent,
    BookListComponent,
    BookAlertsComponent,
    BookDetailsComponent,
    CartComponent,
    AboComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
